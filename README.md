# OpenML dataset: COVID-19_INDIA_Statewise

https://www.openml.org/d/43792

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Coronaviruses are a large family of viruses which may cause illness in animals or humans. In humans, several coronaviruses are known to cause respiratory infections ranging from the common cold to more severe diseases such as MERS and SARS. The most recently discovered coronavirus causes COVID-19 - World Health Organization (WHO).
The number of new cases is increasing day by day around the world. This dataset has information for states of India at a daily level.
Content
COVID-19 cases at a daily level is present in COVID19INDIA.csv file
Acknowledgements
Thanks to the Indian Ministry of Health  Family Welfare for making the data available to the general public.
Thanks to covid19india.org for making the individual level details and testing details available to the general public.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43792) of an [OpenML dataset](https://www.openml.org/d/43792). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43792/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43792/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43792/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

